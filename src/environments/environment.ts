// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA3VxJLDaFvI_BRXjtrZvBURBQA_zIK4u8",
    authDomain: "firechat-39a69.firebaseapp.com",
    databaseURL: "https://firechat-39a69.firebaseio.com",
    projectId: "firechat-39a69",
    storageBucket: "",
    messagingSenderId: "784460301855"
  }
};
