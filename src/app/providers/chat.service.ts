import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Mensaje } from "../interfaces/mensaje.interface";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Alert } from 'selenium-webdriver';

@Injectable()
export class ChatService {
  private itemsCollection: AngularFirestoreCollection<Mensaje>;
  private chats:Mensaje[] = []; 
  private usuario:any = {};

  constructor( private afs: AngularFirestore, private afAuth: AngularFireAuth ) { 
    this.afAuth.authState.subscribe(user=>{
      console.log('Estado del usuario: ', user);
      if(!user){
        return;
      }
      this.usuario.nombre = user.displayName;
      this.usuario.uid = user.uid;
      this.usuario.foto = user.photoURL;
      
    })
  }
  
  login( proveedor:string) {

    switch (proveedor) {
      case 'google':
      this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());    
        break;
      case 'twitter':
        this.afAuth.auth.signInWithPopup(new firebase.auth.TwitterAuthProvider());    
        break; 
      case 'github':
        this.afAuth.auth.signInWithPopup(new firebase.auth.GithubAuthProvider());    
        break;
      case 'telefono':
        this.afAuth.auth.signInWithPopup(new firebase.auth.PhoneAuthProvider());    
        break;
        case 'anonimo':
        this.afAuth.auth.signInAnonymously();
        break;
        case 'facebook':
        this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider());
        break;
    
      default:
        break;
    }
    
  }
  logout() {
    this.usuario = {};
    this.afAuth.auth.signOut();
  }

  cargarMensajes(){
    this.itemsCollection = this.afs.collection<Mensaje>('chats', ref=>ref.orderBy('fecha','desc').limit(5));
    return this.itemsCollection.valueChanges().map( (mensajes:Mensaje[]) =>{
      console.log(mensajes);

      this.chats = [];
      for (let mensaje of mensajes) {
        this.chats.unshift(mensaje);
      }

      return this.chats;
    })
  }
  agregarMensaje( texto:string ){
    // TODO falta e l UID del usuario
    let mensaje:Mensaje = {
      nombre:this.usuario.nombre,
      mensaje:texto,
      fecha: new Date().getTime(),
      uid: this.usuario.uid
    }
    return this.itemsCollection.add(mensaje);

  }

}
